Solus MEGAsync Repository
=========================

This is a 3rd-party Solus repository for the MEGAsync software, not included in the official Solus repositories.

## Usage

+ Add the repository: `sudo eopkg add-repo megasync https://gitlab.com/abdulocracy/solus-megasync-repository/raw/master/eopkg-index.xml.xz`
+ Enable the repository: `sudo eopkg enable-repo megasync`

## Packages

+ `megasync`: Sync your files to your Mega account
+ `nautilus-megasync`: MEGA nautilus extension